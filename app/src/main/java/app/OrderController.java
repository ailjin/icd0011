package app;


import dao.OrderDao;
import model.Order;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OrderController {

    private OrderDao dao;

    public OrderController(OrderDao dao) {
        this.dao = dao;
    }

    @GetMapping("orders")
    public List<Order> getOrders() {
        return dao.getAllOrders();
    }

    @GetMapping("orders/{id}")
    public Order getOrderById(@PathVariable Long id) {
        return dao.getOrderById(id);
    }

    @PostMapping("orders")
    public Order saveOrders(@RequestBody @Valid Order order) {
        return dao.saveOrder(order);
    }

    @DeleteMapping("orders/{id}")
    public void deleteOrders(@PathVariable Long id) {
        dao.deleteOrderById(id);
    }
}
