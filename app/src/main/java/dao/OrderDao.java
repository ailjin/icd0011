package dao;

import model.Order;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@ComponentScan(basePackages = {"conf"})
public class OrderDao {

    @PersistenceContext
    private EntityManager em;

    public Order getOrderById(Long id) {
        TypedQuery<Order> query = em.createQuery("select o from Order o where o.id = :id", Order.class);

        query.setParameter("id", id);

        return query.getSingleResult();
    }

    @Transactional
    public Order saveOrder(Order order) {
        if (order.getId() == null) {
            em.persist(order);
        } else {
            em.merge(order);
        }
        return order;
    }


    public List<Order> getAllOrders() {
        return em.createQuery("select o from Order o", Order.class).getResultList();
    }

    @Transactional
    public void deleteOrderById(Long id) {
        em.createQuery("delete from Order o where o.id=:id")
                .setParameter("id", id)
                .executeUpdate();
    }
}
