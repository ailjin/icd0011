package user;

import java.util.LinkedList;
import java.util.List;

public class UserDao {

    public User getUserByUserName(String userName) {
        switch (userName) {
            case "user": return new User("user", "Anonymous User");
            case "alice": return new User("alice", "Alice Smith");
            case "bob": return new User("bob", "Bob Jones");
            default: return null;
        }
    }

    public List<User> getUsers() {
        return new LinkedList<>();
    }


}
