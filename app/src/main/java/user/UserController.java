package user;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @GetMapping("version")
    public String home() {
        return "Api version url";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("users")
    public List<User> getUsers() {
        return new UserDao().getUsers();
    }

    @PreAuthorize("#userName == authentication.name || hasRole('ROLE_ADMIN')")
    @GetMapping("users/{userName}")
    public User getUserByName(@PathVariable String userName) {
        return new UserDao().getUserByUserName(userName);
    }

}